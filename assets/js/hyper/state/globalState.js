var companyInfo = {
  title: 'PRIME GEORGIA STEAKHOUSE',
  phone: '(770) - 281 - 8004', 
  location: 'Newnan, Georgia',
}

var specialMenuData =[
  {
    title: 'All American Cheeseburger',
    description: 'Fried eggs, Steak, baked potoato or french fries, side of vegetables',
    price: 15
  },
  {
    title: 'Chicken Fingers',
    description: 'Fried eggs, Steak, baked potoato or french fries, side of vegetables',
    price: 25
  },
  {
    title: 'Super BBQ Grill No Smoke',
    description: 'Fried eggs, Steak, baked potoato or french fries, side of vegetables',
    price: 35
  },
   
]

var reviewsData = [
  {
    company: 'Diner Drives and Dives',
    author:'Guy Fieri',
  authorInfo: 'Host Diner Drives and Dives', 
  highlight: '"This is a one way ticket to Flavortown USA!"',
  review: 'Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan.'
},
{
  company: 'Atlanta Journal Constitution',
author: 'Joe Bastiachi',
authorInfo: 'Winner of the Chef Masters', 
highlight: '"This place will put the south in your mouth!"',
review: 'Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan.'
},
{
  company: 'Food Network',
author: 'Alton Brown',
authorInfo: 'Host of Good Eats', 
highlight: '"Hands down the best steak dinner on the planet!"',
review: 'Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan.'
},
{
  company: 'Good Day Atlanta',
author: 'Fred Blakenship',
authorInfo: 'Award Winning Anchor', 
highlight: '"Great food and great service!"',
review: 'Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan.'
},
{
company: 'Southern Cuisines magazine',
author: 'Girgio Manchatti',
authorInfo: 'Lead Editor Southern Cuisines magazine', 
highlight: '#1 Restaurant in the Metro Atlanta Area!',
review: 'Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan.'
},

]

var randomQuoteData = [
  {
    author: 'Maya Angelou',
    quote: 'Im Just Someone Who Likes Cooking And Sharing Food Is A Form Of Expression.',
},

{
  author: 'Anthony Bourdain',
  quote: 'Food is everything we are. Its an extension of nationalist feeling ethnic feeling your personal history your province your region your tribe, your grandma. Its inseparable from those from the get-go.',
},

{
  author: 'Guy Fieri',
  quote: 'Cooking is all about people. Food is maybe the only universal thing that really has the power to bring everyone together. No matter what culture, everywhere around the world, people get together to eat.',
},

]

export const globalState = {
  count: 0,
  companyInfo,
  specialMenuData,
  reviewsData,
  randomQuoteData,
  reviewStatus:{
  currentReview: 0,
}
}

