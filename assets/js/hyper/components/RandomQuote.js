import {h, app} from 'hyperapp'

export default function TopImg({state, actions}) {
  return (
    <section id="RandomQuote" style={{
      backgroundImage:'linear-gradient(135deg,rgba(0, 0, 0, .2) 0, #000 100%),url("https://blog.slice.com/wp-content/uploads/2016/05/spices.jpg")'
    }}>
     <div class="container">
     <h1>"I'm just someone who likes cooking and sharing food is a form of expression."</h1>
     <span class="author">- Maya Angelou </span>  
          </div>
    </section>
  )
}
 