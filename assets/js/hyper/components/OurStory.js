import {h, app} from 'hyperapp'

export default function OurStory({state, actions}) {
  return (
    <section id="OurStory">
    <div class="container">
    <div class="row">
    <div class="col-md-6">
    <h5 class="comp-title">Our Story</h5>
    <h2>"Cooking is the art of Adjustment"</h2>
    <p>
    Lorem ipsum dolor amet hot chicken try-hard photo booth, distillery air plant leggings pug hashtag pickled yuccie asymmetrical. Hammock schlitz prism, lyft health goth godard tousled kogi deep v man bun food truck fam hoodie ugh vegan. Raclette gluten-free twee small batch butcher seitan swag cray pug etsy sustainable.    
    </p>
    <div class="quote"> "The best steak dinner in the World"- <strong>Cleveland Hill IV </strong> </div>
    <a href="#" class="reserve-btn">Reserve</a>
    </div>
    <div class="col-md-6">
    <div class="video-img">
    
    </div>
    </div>
    </div>
    </div>
    </section>
  )
}
 