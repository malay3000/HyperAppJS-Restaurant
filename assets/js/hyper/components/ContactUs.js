import {h, app} from 'hyperapp'

export default function ContactUs({state, actions}) {
  return (
    <section id="ContactUs" class="texturebg">
    <div class="container">
    <h5 class ="comp-title">Contact Us</h5>
    <h2> Prime Georgia Steakhouse</h2>

    <div class="box">
    <div class="row">
    <div class="col-md-6">
    <div class="title">
    Newnan, Georgia   
    </div>
    <h6 class="Address">
      435 Bullsboro Dr. <br/>
    Newnan, GA 30265
    </h6>
  
    <p>
      <strong>email: </strong><a href="info@delriossteakhouse.com">info@delriossteakhouse.com</a>
    </p>


    </div>


    <div class="col-md-6">
 <h6>
   Phone: 
 </h6>
 <div class="title">
 770 - 281 - 8004
 </div>
 <h6>
   Lunch Service: 
 </h6>
 <p>
   Friday, Saturday and Sunday <br/>
   from 12pm - 2:00pm
 </p>
 <h6>
   Dinner Service: 
 </h6>
 <p>
  Daily <br/>
   from 6pm - 9:00pm
 </p>
    </div>

    </div>
    </div>
    </div>
    </section>
  )
}
